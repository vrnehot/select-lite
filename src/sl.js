;(function(){
	'use strict';

	function addEvent(el, event, handler, useCapture){
		if(el.addEventListener){
			el.addEventListener(event, handler, useCapture);
		}
		else if(el.attachEvent){
			el.attachEvent('on' + event, handler);
		}
	}

	function createElement(tag, className, attr, html){
		var el = document.createElement(tag);
		el.className = className || '';
		if(attr){
			for(var i in attr){
				el.setAttribute(i, attr[i]);
			}
		}
		if(html){
			el.innerHTML = html;
		}
		return el;
	}

	function SelectLite(opt){
		this.opt = {};
		var i;
		for(i in this._default){
			this.opt[i] = this._default[i];
		}
		for(i in opt){
			this.opt[i] = opt[i];
		}

		var h = {};
		for(i in this._handlers){
			h[i] = this._handlers[i].bind(this);
		}
		this._handlers = h;

		this.init();
	}

	SelectLite.trackItems = {};
	SelectLite.trackId = 0;
	SelectLite.track = function(el){
		++SelectLite.trackId;
		SelectLite.trackItems[SelectLite.trackId] = el;
		return SelectLite.trackId;
	};

	SelectLite.prototype.init = function(){
		this.createElements();
		this.bindEvents();
		this.updateButton();

		if(this.opt.width === true){
			this.wrap.style.width = this.opt.el.offsetWidth + 'px';
		}
		else if(this.opt.width){
			this.wrap.style.width = this.opt.width + 'px';
		}

		this.opt.el.style.display = 'none';
		this.opt.el.insertAdjacentElement('afterEnd', this.wrap);
	};
	SelectLite.prototype.createElements = function(){
		this.wrap = createElement('span', 'sl-wrap');
		this.button = createElement('span', 'sl-button');
		this.buttonText = createElement('span', 'sl-button-text');
		this.popup = createElement('span', 'sl-popup');
		this.options = [];
		for(var i = 0, l = this.opt.el.length, opt; i < l; ++i){
			opt = createElement('span', 'sl-option' + (this.opt.el[i].selected ? ' sl-selected' : ''), {'data-value': this.opt.el[i].value, 'data-src': SelectLite.track(this.opt.el[i])}, this.opt.el[i].innerHTML);
			this.options.push(opt);
			this.popup.appendChild(opt);
		}
		this.button.appendChild(this.buttonText);
		this.wrap.appendChild(this.button);
		this.wrap.appendChild(this.popup);
	};
	SelectLite.prototype.bindEvents = function(){
		addEvent(this.button, 'click', this._handlers.buttonClick);
		for(var i = 0, l = this.options.length; i < l; ++i){
			addEvent(this.options[i], 'click', this._handlers.optionClick);
		}
		addEvent(document, 'click', this._handlers.outsideClick);
	};

	SelectLite.prototype.isOpen = function(){
		return ~this.wrap.className.indexOf('sl-open');
	};
	SelectLite.prototype.open = function(){
		this.wrap.className += ' sl-open';
		return this;
	};
	SelectLite.prototype.close = function(){
		this.wrap.className = this.wrap.className.replace(this._re.open, '');
		return this;
	};
	SelectLite.prototype.toggle = function(){
		return this.isOpen() ? this.close() : this.open();
	};

	SelectLite.prototype.updateButton = function(){
		var i;
		var l;
		if(this.multiple){
			var titles = [];
			for(i = 0, l = this.options.length; i < l; ++i){
				if(~this.options[i].className.indexOf('sl-selected')){
					titles.push(this.options[i].innerHTML);
				}
			}
			this.updateButtonMultiple(titles);
		}
		else{
			for(i = 0, l = this.options.length; i < l; ++i){
				if(~this.options[i].className.indexOf('sl-selected')){
					this.buttonText.innerHTML = this.options[i].innerHTML;
				}
			}
		}
	};
	SelectLite.prototype.updateButtonMultiple = function(titles){
		if(titles.length > this.opt.multipleMaxTitles){
			this.buttonText.innerHTML = this.opt.multipleTemplate.replace('#', titles.length).replace('%', this.options.length);
		}
		else if(titles.length){
			this.buttonText.innerHTML = titles.join(this.opt.multipleSeparator);
		}
		else{
			this.buttonText.innerHTML = this.opt.emptyTitle || '';
		}
	};

	SelectLite.prototype.destroy = function(){};

	SelectLite.prototype._handlers = {
		buttonClick: function(e){
			this.toggle();
		},
		optionClick: function(e){
			if(this.opt.multiple){
				if(~e.target.className.indexOf('sl-selected')){
					e.target.className = e.target.className.replace(this._re.selected, '');
					SelectLite.trackItems[e.target.getAttribute('data-src')].selected = false;
				}
				else{
					e.target.className += ' sl-selected';
					SelectLite.trackItems[e.target.getAttribute('data-src')].selected = true;
				}
			}
			else{
				for(var i = 0, l = this.options.length; i < l; ++i){
					this.options[i].className = this.options[i].className.replace(this._re.selected, '');
					SelectLite.trackItems[this.options[i].getAttribute('data-src')].selected = false;
				}
				e.target.className += ' sl-selected';
				SelectLite.trackItems[e.target.getAttribute('data-src')].selected = true;
				this.close();
			}
			this.updateButton();
		},
		outsideClick: function(e){
			var el = e.target;
			while(el.parentNode){
				if(el.parentNode == this.wrap) return;
				el = el.parentNode;
			}
			this.close();
		},
	};
	SelectLite.prototype._default = {
		multiple: false,
		multipleTemplate: 'выбрано # из %',
		multipleSeparator: ', ',
		multipleMaxTitles: 3,
		emptyTitle: '',
		width: false,
	};
	SelectLite.prototype._re = {
		open: / sl-open/g,
		selected: / sl-selected/g,
	};

	window.SelectLite = SelectLite;
})();