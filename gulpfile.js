var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');

gulp.task('style', function(){
	return gulp.src('./src/sl.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('dist'));
});

gulp.task('script', function(){
	return gulp.src('./src/*.js')
	.pipe(uglify())
	.pipe(concat('sl.min.js'))
	.pipe(gulp.dest('dist'));
});

gulp.task('public', function(){
	return gulp.src(['./dist/sl.min.js', './dist/sl.css'])
	.pipe(gulp.dest('public'));
});

gulp.task('default', ['style', 'script', 'public']);